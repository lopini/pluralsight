import './App.css';

const colorMap = [
  'blue', 'red', 'yellow', 'pink'
]
const optionsLength = colorMap.length;

const getColor = () => {
  console.log('called');
  const factor = optionsLength;
  const randomChoice = Math.ceil(Math.random() * factor) - 1;
  console.log(`choice was ${randomChoice}`);
  return colorMap[randomChoice];
}


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <div style={{
          color: getColor()
        }}>
          Olá Mundo
        </div>
        <div style={{
          color: getColor()
        }}>
          Olá Mundo
        </div>
      </header>
    </div>
  );
}

export default App;
