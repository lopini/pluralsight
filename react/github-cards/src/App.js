
import './App.css';
import React from 'react';

// TODO: Transform the card component into a function based component
// TODO: Handle errors properly

const testData = [
    {name: "Dan Abramov", avatar_url: "https://avatars0.githubusercontent.com/u/810438?v=4", company: "@facebook"},
    {name: "Sophie Alpert", avatar_url: "https://avatars2.githubusercontent.com/u/6820?v=4", company: "Humu"},
    {name: "Sebastian Markbåge", avatar_url: "https://avatars2.githubusercontent.com/u/63648?v=4", company: "Facebook"},
];


const CardList = (props) => {
    return (
        <div>
            { props.profiles.map((profile) => <Card key={profile.id} {...profile}/>) }
        </div>
    )
}

class Form extends React.Component {

    state = {
        username: ''
    }

    searchUser = async (username = '') => {
        const USERS_ENDPOINT = 'https://api.github.com/users';
        const URI = `${USERS_ENDPOINT}/${username}`;

        try {
            const response = await fetch(URI);
            if (!response.ok)
                console.warn(`${URI} responded ${response.status}`);

            const json = await response.json();
            console.debug(json)
            return json;
        }
        catch (e) {
            console.error(`unexpected error ${e}`);
        }
    }

    handleSubmit = async event => {
        event.preventDefault();

        const username = this.state.username;
        console.log(`searching for ${username}`);
        const user = await this.searchUser(username);

        if (user) {
            this.props.onSubmit(user);
            this.setState({username: ''})
        }
    }

    render() {
        return (
            <form action="" onSubmit={this.handleSubmit}>
                <input type="text"
                       placeholder="username"
                       value={this.state.username}
                       onChange={event => this.setState({username: event.target.value})}
                />
                <button>Add</button>
            </form>
        )
    }
}

class Card extends React.Component {
    render() {

        const profile = this.props;

        return (
            <div className="github-profile">
                <img src={profile.avatar_url}/>
                <div className="info">
                    <div className="name">{profile.name || profile.login}</div>
                    <div className="company">{profile.company || profile.location}</div>
                </div>
            </div>
        )
    }
}

class App extends React.Component {

    state = {
        profiles: testData
    }

    // alternative way
    // constructor() {
    //     super();
    //     this.state = {
    //         'profiles': testData
    //     }
    // }

    handleUserAddedOnList = userAdded => {
        this.setState(
            prevState => ({ profiles: [...prevState.profiles, userAdded] })
        );
    };

    render() {
        return (
            <div>
                <div className="header">
                    {this.props.title}
                </div>
                <Form onSubmit={this.handleUserAddedOnList} />
                <CardList profiles={this.state.profiles} />
            </div>
        )
    }
}



export default App;
